import serial
import binascii
import mysql.connector

ser=serial.Serial('/dev/ttyUSB0')#Ouverture du port série
print('Port série OK')
db = mysql.connector.connect(host="localhost",user="root",password="admin",database="serrure")#Connection à la base de donnée
cursor = db.cursor()#définition d'un curseur pour les requétes
print('Base de donnée connectée')

while True:
    #Lecture et traitement du port série 
    line=ser.readline()
    code_recu=line.split(",".encode('utf-8'))
    code_traite=binascii.hexlify(code_recu[1])#Décodage b'\x00X\x13\xad\xe6' --> b'005813ade6'
    print("code reçu : ",code_traite)
    validation=0#mise à 0 de la condition
    #Requète
    query = ("SELECT * FROM `utilisateur` WHERE 1")
    cursor.execute(query)
    #Vérification base de donnée
    for (valeur) in cursor :
        code_bdd=valeur[3].encode('utf-8')
        print("code base de donnée :",code_bdd)
        if(code_bdd == code_traite) :
            validation=1
            
    if(validation==1):
        autorisation=b'1'#validé
    else :
        autorisation=b'0'#validé
    print("Coté :",code_recu[2])
    print("Autorisée",autorisation)
    ser.write(code_recu[2])
    ser.write(autorisation)
    ser.reset_input_buffer()
    print('Fin')
    cursor.close()
db.close()

    
