/*******************************************************************************
* File Name: raspi.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_raspi_H)
#define CY_ISR_raspi_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void raspi_Start(void);
void raspi_StartEx(cyisraddress address);
void raspi_Stop(void);

CY_ISR_PROTO(raspi_Interrupt);

void raspi_SetVector(cyisraddress address);
cyisraddress raspi_GetVector(void);

void raspi_SetPriority(uint8 priority);
uint8 raspi_GetPriority(void);

void raspi_Enable(void);
uint8 raspi_GetState(void);
void raspi_Disable(void);

void raspi_SetPending(void);
void raspi_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the raspi ISR. */
#define raspi_INTC_VECTOR            ((reg32 *) raspi__INTC_VECT)

/* Address of the raspi ISR priority. */
#define raspi_INTC_PRIOR             ((reg8 *) raspi__INTC_PRIOR_REG)

/* Priority of the raspi interrupt. */
#define raspi_INTC_PRIOR_NUMBER      raspi__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable raspi interrupt. */
#define raspi_INTC_SET_EN            ((reg32 *) raspi__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the raspi interrupt. */
#define raspi_INTC_CLR_EN            ((reg32 *) raspi__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the raspi interrupt state to pending. */
#define raspi_INTC_SET_PD            ((reg32 *) raspi__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the raspi interrupt. */
#define raspi_INTC_CLR_PD            ((reg32 *) raspi__INTC_CLR_PD_REG)


#endif /* CY_ISR_raspi_H */


/* [] END OF FILE */
