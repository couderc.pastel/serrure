/*******************************************************************************
* File Name: rfid.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_rfid_H)
#define CY_ISR_rfid_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void rfid_Start(void);
void rfid_StartEx(cyisraddress address);
void rfid_Stop(void);

CY_ISR_PROTO(rfid_Interrupt);

void rfid_SetVector(cyisraddress address);
cyisraddress rfid_GetVector(void);

void rfid_SetPriority(uint8 priority);
uint8 rfid_GetPriority(void);

void rfid_Enable(void);
uint8 rfid_GetState(void);
void rfid_Disable(void);

void rfid_SetPending(void);
void rfid_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the rfid ISR. */
#define rfid_INTC_VECTOR            ((reg32 *) rfid__INTC_VECT)

/* Address of the rfid ISR priority. */
#define rfid_INTC_PRIOR             ((reg8 *) rfid__INTC_PRIOR_REG)

/* Priority of the rfid interrupt. */
#define rfid_INTC_PRIOR_NUMBER      rfid__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable rfid interrupt. */
#define rfid_INTC_SET_EN            ((reg32 *) rfid__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the rfid interrupt. */
#define rfid_INTC_CLR_EN            ((reg32 *) rfid__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the rfid interrupt state to pending. */
#define rfid_INTC_SET_PD            ((reg32 *) rfid__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the rfid interrupt. */
#define rfid_INTC_CLR_PD            ((reg32 *) rfid__INTC_CLR_PD_REG)


#endif /* CY_ISR_rfid_H */


/* [] END OF FILE */
