/*******************************************************************************
* File Name: RxInterrupt.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_RxInterrupt_H)
#define CY_ISR_RxInterrupt_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void RxInterrupt_Start(void);
void RxInterrupt_StartEx(cyisraddress address);
void RxInterrupt_Stop(void);

CY_ISR_PROTO(RxInterrupt_Interrupt);

void RxInterrupt_SetVector(cyisraddress address);
cyisraddress RxInterrupt_GetVector(void);

void RxInterrupt_SetPriority(uint8 priority);
uint8 RxInterrupt_GetPriority(void);

void RxInterrupt_Enable(void);
uint8 RxInterrupt_GetState(void);
void RxInterrupt_Disable(void);

void RxInterrupt_SetPending(void);
void RxInterrupt_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the RxInterrupt ISR. */
#define RxInterrupt_INTC_VECTOR            ((reg32 *) RxInterrupt__INTC_VECT)

/* Address of the RxInterrupt ISR priority. */
#define RxInterrupt_INTC_PRIOR             ((reg8 *) RxInterrupt__INTC_PRIOR_REG)

/* Priority of the RxInterrupt interrupt. */
#define RxInterrupt_INTC_PRIOR_NUMBER      RxInterrupt__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable RxInterrupt interrupt. */
#define RxInterrupt_INTC_SET_EN            ((reg32 *) RxInterrupt__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the RxInterrupt interrupt. */
#define RxInterrupt_INTC_CLR_EN            ((reg32 *) RxInterrupt__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the RxInterrupt interrupt state to pending. */
#define RxInterrupt_INTC_SET_PD            ((reg32 *) RxInterrupt__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the RxInterrupt interrupt. */
#define RxInterrupt_INTC_CLR_PD            ((reg32 *) RxInterrupt__INTC_CLR_PD_REG)


#endif /* CY_ISR_RxInterrupt_H */


/* [] END OF FILE */
